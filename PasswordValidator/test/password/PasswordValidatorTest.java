package password;

/*
 * @author Jason Xu, 991545529
 * 
 * Assumption is that spaces are not considered valid characters
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testHasEnoughDigits() {
		assertTrue("Not enough digits", PasswordValidator.hasEnoughDigits("1234abcdef"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Not enough digits", PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Not enough digits", PasswordValidator.hasEnoughDigits("12abcdef"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Not enough digits", PasswordValidator.hasEnoughDigits("1abcdefg"));
	}
	
	@Test
	public void testHasUpperCase() {
		assertTrue("Does not have uppercase & lowercase character", PasswordValidator.hasUpperCase("DEF123abc"));
	}
	
	@Test
	public void testHasUpperCaseException() {
		assertFalse("Does not have uppercase & lowercase character", PasswordValidator.hasUpperCase(null));
	}
	
	@Test
	public void testHasUpperCaseBoundaryIn() {
		assertTrue("Does not have uppercase & lowercase character", PasswordValidator.hasUpperCase("123456aB"));
	}
	
	@Test
	public void testHasUpperCaseBoundaryOut() {
		assertFalse("Does not have uppercase & lowercase character", PasswordValidator.hasUpperCase("12345678"));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Test
	public void testIsValidLength() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test 
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null)); 
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	 
	@Test 
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567")); 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	 
	
	/*
	 * @Test public void testIsValidLengthExceptionSpaces() {
	 * assertFalse("Invalid password length",
	 * PasswordValidator.isValidLength("          ")); }
	 */
	

	

}
