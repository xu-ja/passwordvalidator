package password;

import java.util.regex.Pattern;

/*
 * @author Jason Xu, 991545529
 * 
 * This class validates passwords and it will be developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	/*
	 * Validates length of password, blank spaces are not considered valid characters
	 * @param password
	 * @return
	 */

	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasEnoughDigits(String password) {
		if (password != null) {
			int numDigits = 0;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isDigit(password.charAt(i))) {
					numDigits++;
				}
			}
			if (numDigits >= MIN_DIGITS) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasUpperCase(String password) {
		if (password != null) {
			if (password.matches(".*[A-Z].*") && password.matches(".*[a-z].*")) {
				return true;
			}
		}
		return false;
	}

}
